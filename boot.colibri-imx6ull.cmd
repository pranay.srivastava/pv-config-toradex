env set baseargs 'root=/dev/ram rootfstype=ramfs init=/init ubi.mtd=ubi'
env set loadfdt 'load ${devtype} ${devnum}:2 ${fdt_addr_r} "/trails/${boot_rev}/.pv/pv-fdt.dtb"'
env set loadargs 'setenv bootargs "${mtdparts} ${baseargs} pv_try=${pv_try} pv_rev=${boot_rev} panic=2 ${args} ${localargs}"'
env set loadenv_mmc 'load mmc ${devnum}:2 ${loadaddr} /boot/uboot.txt'
env set loadenv_ubi 'ubifsload ${loadaddr} /boot/uboot.txt'
env set loadenv 'if test "${pv_fstype}" = "ubi"; then run loadenv_ubi; else run loadenv_mmc; fi; setenv pv_try; env import ${loadaddr} 0x400; if env exists pv_try; then if env exists pv_trying && test ${pv_trying} = ${pv_try}; then setenv pv_trying; saveenv; setenv boot_rev ${pv_rev}; else setenv pv_trying ${pv_try}; saveenv; setenv boot_rev ${pv_trying}; fi; else setenv boot_rev ${pv_rev}; fi;'
env set loadstep_mmc 'run loadenv; load mmc ${devnum}:2 ${kernel_addr_r} /trails/${boot_rev}/.pv/pv-kernel.img; load mmc ${devnum}:2 ${ramdisk_addr_r} /trails/${boot_rev}/.pv/pv-initrd.img; setenv ${ramdisk_size} ${filesize};'
env set loadstep_ubi 'ubi part ${pv_ubipart}; ubifsmount ubi0:${pv_ubirootfs}; run loadenv; ubifsload ${kernel_addr_r} /trails/${boot_rev}/.pv/pv-kernel.img; ubifsload ${ramdisk_addr_r} /trails/${boot_rev}/.pv/pv-initrd.img;'
env set loadstep 'if test "${pv_fstype}" = "ubi"; then run loadstep_ubi; else run loadstep_mmc; fi; setenv ramdisk_size ${filesize}'
env set bootcmd_run 'run loadstep; run loadfdt; run loadargs; bootz ${kernel_addr_r} ${ramdisk_addr_r}:${ramdisk_size} ${fdt_addr_r}; echo "Failed to boot step, rebooting"; sleep 1; reset'
env set pv_fstype "ubi"
env set pv_ubipart "ubi"
env set pv_ubirootfs "pvroot"
run bootcmd_run

